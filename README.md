# README #

Simple snake game developed in Unity. 
Control snake movement with arrow keys, suitable for Windows and Mac.
To start game, in case Unity shows a blank scene, go to MainMenu scene, press play and choose a difficulty. Or build and run game.
Ideal aspect ratio 16:9.
